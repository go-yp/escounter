package main

import (
	"io"
	"strings"
	"sync"
	"testing"

	es8 "github.com/elastic/go-elasticsearch/v8"

	"github.com/stretchr/testify/require"
)

func TestCounterSequence(t *testing.T) {
	es, err := es8.NewDefaultClient()
	require.NoError(t, err)

	const viewsIndex = "views"
	// language=JSON
	const viewsMapping = `{            
    "properties": {
        "id": {
            "type": "number"
        },
        "views": {
            "type": "number"
        }
    }
}`

	resetMapping(t, es, viewsIndex, viewsMapping)

	const documentID = "1"
	// language=JSON
	const document = `{
	"id":"1",
	"views":0
}`

	createDocument(t, es, viewsIndex, documentID, document)

	printDocument(t, es, viewsIndex, documentID)

	const count = 1000

	{
		// language=JSON
		const updateScript = `{
	"script": {
		"lang":   "painless",
		"source": "ctx._source.views += 1;"
	},
	"query": {
		"term": {
			"_id": "1"
		}
	}
}`

		for i := 0; i < count; i++ {
			updateByQuery(t, es, viewsIndex, updateScript)
		}
	}

	printDocument(t, es, viewsIndex, documentID)
}

func TestCounterParallel(t *testing.T) {
	es, err := es8.NewDefaultClient()
	require.NoError(t, err)

	const viewsIndex = "views"
	// language=JSON
	const viewsMapping = `{            
    "properties": {
        "id": {
            "type": "number"
        },
        "views": {
            "type": "number"
        }
    }
}`

	resetMapping(t, es, viewsIndex, viewsMapping)

	const documentID = "1"
	// language=JSON
	const document = `{
	"id":"1",
	"views":0
}`

	createDocument(t, es, viewsIndex, documentID, document)

	printDocument(t, es, viewsIndex, documentID)

	const count = 1000

	{
		// language=JSON
		const updateScript = `{
	"script": {
		"lang":   "painless",
		"source": "ctx._source.views += 1;"
	},
	"query": {
		"term": {
			"_id": "1"
		}
	}
}`

		const (
			parallel = 2
		)

		var (
			wg   sync.WaitGroup
			rate = make(chan struct{}, parallel)
		)

		for i := 0; i < count; i++ {
			wg.Add(1)

			rate <- struct{}{}

			go func() {
				defer func() {
					_ = <-rate

					wg.Done()
				}()

				updateByQuery(t, es, viewsIndex, updateScript)
			}()
		}

		wg.Wait()
	}

	printDocument(t, es, viewsIndex, documentID)
}

func resetMapping(t *testing.T, es *es8.Client, index, viewsMapping string) {
	es.Indices.Delete([]string{index})

	_, err := es.Indices.PutMapping(
		[]string{index},
		strings.NewReader(viewsMapping),
	)
	require.NoError(t, err)
}

func createDocument(t *testing.T, es *es8.Client, viewsIndex string, documentID string, document string) {
	_, err := es.Create(
		viewsIndex,
		documentID,
		strings.NewReader(document),
		// WithRefresh
		es.Create.WithRefresh("true"),
	)
	require.NoError(t, err)
}

func updateByQuery(t *testing.T, es *es8.Client, viewsIndex string, updateScript string) {
	_, err := es.UpdateByQuery(
		[]string{viewsIndex},
		es.UpdateByQuery.WithBody(strings.NewReader(updateScript)),
		// WithRefresh
		es.UpdateByQuery.WithRefresh(true),
	)

	require.NoError(t, err)
}

func printDocument(t *testing.T, es *es8.Client, index, id string) {
	response, err := es.Get(
		index,
		id,
		es.Get.WithPretty(),
		// WithRefresh
		es.Get.WithRefresh(true),
	)
	require.NoError(t, err)
	defer response.Body.Close()

	content, err := io.ReadAll(response.Body)
	require.NoError(t, err)
	t.Log(string(content))
}
