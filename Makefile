up:
	sudo docker-compose up -d

down:
	sudo docker-compose down

test:
	go test . -v -count=1
